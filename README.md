#  Mon chat virtuel
https://monchatvirtuel-main.netlify.app/main.html

# Description
C'est un jeu dans lequel il faut s'occuper d'un chaton jusqu'à qu'il devienne un chat adulte équilibré. Il faut faire attention à son alimentation, sa santé et veiller à ce qu'il ne s'ennuie pas. En jouant à des petits jeux avec lui, on peut gagner des points qui nous serviront à lui acheter des cadeaux.  

# Visuals
<version mobile>

![Capture_d_écran_2023-01-17_à_22.31.56](/uploads/b850a7d2824addfeef7a72c457525592/Capture_d_écran_2023-01-17_à_22.31.56.png)
<version pc>

![Capture_d_écran_2023-01-17_à_22.55.35](/uploads/3769de168d7a8128681092f4a45108b5/Capture_d_écran_2023-01-17_à_22.55.35.png)

# Usage
1.opening Section: On doit donner un nom à son chat et commencer le jeu (Nom:obligatoire)
2.main Section: 4 actions principales (nourrir,soins,jeux,cadeau)
-nourrir: menu avec plusieurs options pour nourrir notre chat. 
-soins: menu qui propose différents types de soins pour notre chat. Nettoyer sa caisse( quand l'icône'caca' apparait), Brosser, l'emmener chez le véto (quand l'icône'virus' apparait)
-jeux:Chifumi,Machine à sous,Baballe:On peux gagner des points avec ces jeux
-cadeau:O petite boutique où on peut acheter un cadeau à notre chat avec les points obtenus durant les jeux. 

# Support
En cas de questions :
*email:hisamistolz@gmail.com
*gitlab: @Hisami

# Author
Hisami Stolz
