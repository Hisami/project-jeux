export interface Cat{
    name:string,
    days:number,
    stage:Stage,
    condition:Condition;
    totalPoint:Pointtotal;
    commitCounter:number;
    pointGift:number;
}

export interface Stage{
    devStage:string,
    image:string
}

export interface Condition {
    hungryLevel:number;
    satisfactionLevel:number;
    healthLevel:number;
    intelligenceLevel:number
}

export interface Pointtotal{
    hungryTotal:number;
    satisfactionTotal:number;
    healthLevelTotal:number;
    intelligenceLevelTotal:number;
    affection:number;
}

export interface Feed{
    effect?:()=> void;
    item:string;
    itemImg:string;
    minusPoint:Condition;
    plusPoint:Condition;
    reaction:Reaction;
}

export interface Reaction{
    reactionTxt:string;
    reactionImg:string;
}