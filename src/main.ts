import { Cat, Stage, Condition, Pointtotal, Feed, Reaction } from "./entities"

/* import images */

import babychat from "./image/babycat.png"
import prettychat from "./image/chatmignon.png"
import adultchat from "./image/chatadult.png"
import milk from "./image/drink_milk_gallon.png"
import catfood from "./image/pet_esa_sara_full.png"
import cattreat from "./image/food_beef_jerky.png"
import chocolate from "./image/valentinesday_itachoco2.png"
import toilet from "./image/toilet_sheet_fuku.png"
import brush from "./image/pet_hair_brush.png"
import takeVt from "./image/pet_doctor.png"
import janken from "./image/janken_boys.png"
import slot from "./image/money_slot_machine.png"
import ball from "./image/tennis_animal_neko.png"
import giftmouse from "./image/illustrain10-neko07-300x300.png"
import giftcushion from "./image/kagu_cushion.png"
import giftcattree from "./image/illustrain10-neko06.png"

/* Settings*/
const nameBtn = document.querySelector<HTMLButtonElement>("#namebtn")
const nameInput = document.querySelector<HTMLInputElement>("#catName")
const openingH2 = document.querySelectorAll<HTMLElement>("h2");
const openingImg = document.querySelector<HTMLImageElement>("#imgbebechat")
const openingSection = document.querySelector<HTMLElement>("#opening");
const playSection = document.querySelector<HTMLElement>("#play");
let name = "";
const imageOfcat = document.querySelector<HTMLImageElement>("#imageOfcat");
const catComment = document.querySelector<HTMLElement>('#catComment');
let pointgift = document.querySelector<HTMLDivElement>("#pointgift");
let gameisStart = false;

/* cat par défaut qui définit une première condition au début  */
let defaultcat: Cat = {
    name: "default",
    days: 0,
    stage: {
        devStage: "petit chaton",
        image: babychat
    },
    condition: {
        hungryLevel: 0,
        satisfactionLevel: 0,
        healthLevel: 0,
        intelligenceLevel: 0
    },
    totalPoint: {
        hungryTotal: 0,
        satisfactionTotal: 0,
        healthLevelTotal: 0,
        intelligenceLevelTotal: 0,
        affection: 0
    },
    commitCounter: 0,
    pointGift: 20
}

//Section opening
nameBtn?.addEventListener('click', () => {
    if (nameInput?.value == "") {
        alert("Donne moi un prénom")
        return;
    }
    if (nameInput && openingH2 && nameBtn) {
        name = nameInput.value
        defaultcat.name = name;
        settingSection(defaultcat)
        openingH2[0].textContent = "Enchanté!!";
        openingH2[1].textContent = `Je suis ${nameInput.value}`;
        openingImg?.classList.add("poyon")
        nameInput.classList.add("d-none");
        nameBtn.classList.add("d-none");
    }
    setTimeout(() => {
        changeSection(openingSection, playSection)
    }, 3000);
})

/**
*  Fonction qui va ajouter/rajouter ('d-none')
* @demo1 :une div Type(Element) qui va disparaitre
* @demo2  :une div Type(Element) qui va apparaitre sur l'ecran
*/
function changeSection(demo1: any, demo2: any) {
    demo1.classList.add("d-none");
    demo2.classList.remove("d-none")
}

/**
* Fonction qui va changer et afficher le contenu html selon l'objet 
* @param cat  l'objet dont interface Cat
*/
function settingSection(cat: Cat): void {
    console.log(defaultcat.commitCounter)
    const nameSpan = document.querySelector("#catname");

    if (nameSpan) {
        nameSpan.innerHTML = '';
        nameSpan.textContent = cat.name;
    }
    const daysSpan = document.querySelector("#days")
    if (daysSpan) {
        daysSpan.innerHTML = '';
        daysSpan.textContent = `${cat.days}`;
    }

    const stomachSpan = document.querySelector("#stomach");
    showHeart(stomachSpan, cat.condition.hungryLevel);

    const satisfactionSpan = document.querySelector("#satisfaction");
    showHeart(satisfactionSpan, cat.condition.satisfactionLevel)

    const healthSpan = document.querySelector("#health");
    showHeart(healthSpan, cat.condition.healthLevel);

    const intelligenceSpan = document.querySelector("#intelligence");
    showHeart(intelligenceSpan, cat.condition.intelligenceLevel);

    if (imageOfcat) {
        imageOfcat.src = cat.stage.image;
    }

    if (catComment) {
        catComment.textContent = ` ${cat.stage.devStage}`
    }
    if (pointgift) {
        pointgift.textContent = `${cat.pointGift}`
    }
}

/**
* Fonction qui va changer et afficher une icône de coeur (maximum:5) sur html selon 4 différents chiffres dans 'condition' de l'objet Cat
* @spanName 'span' qui affiche les coeurs
* @condition 'number'  de (hungryLevel,satisfactionLevel,healthLevel,intelligenceLevel) dans 'condition' of un objet :Cat
*/
function showHeart(spanName: Element | null, condition: number): void {
    let heartNumber = "";
    if (spanName) {
        spanName.innerHTML = '';
        if (condition < 0) {
            heartNumber = ''
        }
        if (condition <= 5) {
            for (let i = 0; i < condition; i++) {
                heartNumber += '<i class="bi bi-heart-fill"></i>'
            }
        } if (condition > 5) {
            for (let i = 0; i < 5; i++) {
                heartNumber += '<i class="bi bi-heart-fill"></i>'
            }
        }
        spanName.innerHTML = heartNumber
    }
    heartNumber = " "
}

//images apparues sur l'écran principal;
// caca de chat
const imageUnchi = document.querySelector("#unchi");
let imageUnchiExist = false;
setInterval(() => {
    if (imageUnchiExist == false) {
        imageUnchi?.classList.remove("move")
        imageUnchi?.classList.remove("d-none")
        imageUnchiExist = true;
        defaultcat.condition.healthLevel -= 1;
        defaultcat.condition.satisfactionLevel -= 1;
        settingSection(defaultcat)
    }
}, 30000)

//virus 
const imageVirus = document.querySelector("#virus");
let virusExist = false;
setInterval(() => {
    if (virusExist == false) {
        imageVirus?.classList.remove("move")
        imageVirus?.classList.remove("d-none")
        virusExist = true;
        defaultcat.condition.healthLevel -= 3;
        defaultcat.condition.satisfactionLevel -= 3;
        settingSection(defaultcat)
    }
}, 60000)

/**
* Fonction qui va changer chaque chiffre dans 'condition'(hungryLevel, hungryTotal) de l'objet Cat 
* @num1:number,Points en moins,on va diminuer 'num1' de plusieurs unités de son hungryLevel et hungryTotal
* @num2:number,Points supplémentaires,on va ajouter 'num2' plusieurs unités au score de son hungryLevel et hungryTotal
*/
function changeHungrylevel(num1: number, num2: number) {
    defaultcat.condition.hungryLevel = defaultcat.condition.hungryLevel - num1 + num2
    defaultcat.totalPoint.hungryTotal = defaultcat.totalPoint.hungryTotal - num1 + num2
    if (defaultcat.condition.hungryLevel < 0) {
        defaultcat.condition.hungryLevel = 0;
    }
}
// par rapport à "satisfactionLevel","satisfactionTotal"
function changeSatisfactionlevel(num1: number, num2: number) {
    defaultcat.condition.satisfactionLevel = defaultcat.condition.satisfactionLevel - num1 + num2
    defaultcat.totalPoint.satisfactionTotal = defaultcat.totalPoint.satisfactionTotal - num1 + num2
}
// par rapport à "healthLevel","healthTotal"
function changeHealthlevel(num1: number, num2: number) {
    defaultcat.condition.healthLevel = defaultcat.condition.healthLevel - num1 + num2
    defaultcat.totalPoint.healthLevelTotal = defaultcat.totalPoint.healthLevelTotal - num1 + num2
}
// par rapport à "intelligenceLevel","intelligenceTotal"
function changeInglevel(num1: number, num2: number) {
    defaultcat.condition.intelligenceLevel = defaultcat.condition.intelligenceLevel - num1 + num2
    defaultcat.totalPoint.intelligenceLevelTotal = defaultcat.totalPoint.intelligenceLevelTotal - num1 + num2
}


// mettre à jour (ajouter un jour et mettre à jour la condition selon le temps passé)
setInterval(renewDays, 10000)
/**
* Fonction qui va ajouter un jour et d'autres éléments dans 'condition','totalPoint' selon le temps passé
* et le niveau d'engagement sauf 'gameisStart:true';
* @spanName 'span' qui affiche les coeurs
* @condition 'number'  de (hungryLevel,satisfactionLevel,healthLevel,intelligenceLevel) dans 'condition' de l'objet :Cat
*/
function renewDays(): void {
    if (gameisStart) {
        return;
    }
    defaultcat.days += 1;
    console.log(defaultcat.totalPoint)
    if (defaultcat.commitCounter > 0 && defaultcat.commitCounter < 2) {
        defaultcat.totalPoint.affection += 1
    } else if (defaultcat.commitCounter > 3) {
        defaultcat.totalPoint.affection += 2
    }
    defaultcat.commitCounter = 0

    if (defaultcat.condition.hungryLevel > 0) {
        defaultcat.condition.hungryLevel -= 1;
    } else {
        defaultcat.condition.hungryLevel = 0;
        defaultcat.totalPoint.hungryTotal -= 1
    }

    if (imageUnchiExist) {
        if (defaultcat.condition.healthLevel > 0) {
            defaultcat.condition.healthLevel -= 1
        } else {
            defaultcat.condition.healthLevel = 0
        }
        defaultcat.totalPoint.satisfactionTotal -= 1
        defaultcat.totalPoint.healthLevelTotal -= 1
    }
    if (virusExist) {
        if (defaultcat.condition.healthLevel > 0) {
            defaultcat.condition.healthLevel -= 1
        } else {
            defaultcat.condition.healthLevel = 0
        }
        defaultcat.totalPoint.healthLevelTotal -= 1
        defaultcat.totalPoint.satisfactionTotal -= 1
    }

    if (defaultcat.days > 7) {
        defaultcat.stage.devStage = "chat mignon";
        defaultcat.stage.image = prettychat;
        judgeCat()
    }
    if (defaultcat.days > 12) {
        judgeCat()
    }
    if (defaultcat.days > 18) {
        defaultcat.stage.devStage = "chat adult";
        defaultcat.stage.image = adultchat;
        judgeCat()
    }
    if (defaultcat)
        settingSection(defaultcat)
}

/**
* Fonction qui va changer les valeurs de l'objet Cat('stage.devStage','stage.devImg') selon  'totalPoint' dans 'condition'
* de l'objet Cat
*/
let catdisappeared = false;
function judgeCat(): void {
    if (defaultcat.totalPoint.affection == 0) {
        defaultcat.stage.devStage = "cat disappeared"
        defaultcat.stage.image = " "
        if (catComment) {
            catComment.textContent = "Il préféres passer son temps chez le voisin.."
        }
        tryagain();
    } else {
        if (defaultcat.totalPoint.healthLevelTotal < -10) {
            defaultcat.stage.devStage = "unhealthy cat"
            defaultcat.stage.image = "https://2.bp.blogspot.com/-LTwgf6EcY9U/XAY5ngaD6_I/AAAAAAABQc8/-vAsEpKsqQYbXkugGWKFQzjV0LGBHpbcwCLcBGAs/s800/cat02_moyou_white.png"
        }
        if (defaultcat.totalPoint.hungryTotal < -10 || defaultcat.totalPoint.healthLevelTotal > -10) {
            defaultcat.stage.devStage = "hungry cat"
            defaultcat.stage.image = "https://2.bp.blogspot.com/-LTwgf6EcY9U/XAY5ngaD6_I/AAAAAAABQc8/-vAsEpKsqQYbXkugGWKFQzjV0LGBHpbcwCLcBGAs/s800/cat02_moyou_white.png"
        }
        if (defaultcat.totalPoint.intelligenceLevelTotal > 20 && defaultcat.totalPoint.healthLevelTotal > 10 && defaultcat.totalPoint.hungryTotal > 10) {
            defaultcat.stage.devStage = "super cat"
        }
    }
}

const restartbtn = document.querySelector<HTMLButtonElement>("#restartbtn");
/* function pour recommencer le jeux de debut */
function tryagain() {
    restartbtn?.classList.remove("d-none");
    restartbtn?.addEventListener("click", () => {
        changeSection(playSection, openingSection)
    })
}

/*  Section main (Button Section):les boutons cliquables qui font apparaître les modal qui contiennent les listes  */
// changement de l'élément affiché sur l'écran selon 'click' de chaque div
// feed 
let feedBtn = document.querySelector("#feedbtn");
let modalFeed = document.querySelector("#modalFeed")
let listFeeddiv = document.querySelector("#listFeed");
const returnbtn = document.querySelector('#returnbtn')
feedBtn?.addEventListener('click', () => {
    imageOfcat?.classList.remove("movecat")
    if (modalFeed) {
        changeSection(feedBtn, modalFeed)
    }
})
returnbtn?.addEventListener('click', () => {
    changeSection(modalFeed, feedBtn)
    imageOfcat?.classList.add("movecat")
})
//health
let healthBtn = document.querySelector("#healthbtn");
let modalHealth = document.querySelector("#modalHealth")
let listHealthdiv = document.querySelector("#listHealth");
const returnbtn2 = document.querySelector('#returnbtn2')
healthBtn?.addEventListener('click', () => {
    imageOfcat?.classList.remove("movecat")
    if (modalHealth) {
        changeSection(healthBtn, modalHealth)
    }
})
returnbtn2?.addEventListener('click', () => {
    changeSection(modalHealth, healthBtn)
    imageOfcat?.classList.add("movecat")
})
//play game
let playBtn = document.querySelector("#playbtn");
let modalPlay = document.querySelector("#modalPlay")
let listPlaydiv = document.querySelector("#listPlay");
const returnbtn3 = document.querySelector('#returnbtn3')
playBtn?.addEventListener('click', () => {
    imageOfcat?.classList.remove("movecat")
    if (modalHealth) {
        changeSection(playBtn, modalPlay)
    }
})
returnbtn3?.addEventListener('click', () => {
    changeSection(modalPlay, playBtn)
    imageOfcat?.classList.add("movecat")
})
// gift
let giftBtn = document.querySelector("#giftbtn");
let modalGift = document.querySelector("#modalGift")
let listGiftdiv = document.querySelector("#listGift");
const returnbtn4 = document.querySelector('#returnbtn4')
giftBtn?.addEventListener('click', () => {
    imageOfcat?.classList.remove("movecat")
    if (modalGift) {
        changeSection(giftBtn, modalGift)
    }
})
returnbtn4?.addEventListener('click', () => {
    changeSection(modalGift, giftBtn)
    imageOfcat?.classList.add("movecat")

})

// List des objets concernant la partie feed(type:Feed)
let feeds: Feed[] = [
    {
        item: "Lait", itemImg: milk, minusPoint: { hungryLevel: 0, satisfactionLevel: 1, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 1, satisfactionLevel: 2, healthLevel: 1, intelligenceLevel: 0 }, reaction: {
            reactionTxt: "Je suis content !", reactionImg: "https://2.bp.blogspot.com/-mgnaaQREB-4/Wp0Nw_J9ziI/AAAAAAABKjM/ssGvsC0KpzIEeMQh9eISlGBnUlFqHmAuACLcBGAs/s450/pet_honyubin_cat_utsubuse.png"
        }
    },
    {
        item: "Croquettes", itemImg: catfood, minusPoint: { hungryLevel: 0, satisfactionLevel: 1, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 3, satisfactionLevel: 2, healthLevel: 1, intelligenceLevel: 0 }, reaction: {
            reactionTxt: "C'est délicieux !", reactionImg: "http://1.bp.blogspot.com/-k4oA2IEQ5Jg/Viio5duqhWI/AAAAAAAAzqo/dU8GGJNWB3Q/s180-c/cat_pet_esa.png"
        }
    },
    {
        item: "Gourmandise", itemImg: cattreat, minusPoint: { hungryLevel: 0, satisfactionLevel: 1, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 1, satisfactionLevel: 2, healthLevel: 1, intelligenceLevel: 0 }, reaction: {
            reactionTxt: "Encore !", reactionImg: "https://1.bp.blogspot.com/-vj_Y95GLJ2Q/Vq88xMkGq6I/AAAAAAAA3fM/1OTT2HZbO_0/s400/pet_oyatsu_cat.png"
        }
    },
    {
        item: "Chocolat", itemImg: chocolate, minusPoint: { hungryLevel: 0, satisfactionLevel: 0, healthLevel: 3, intelligenceLevel: 2 }, plusPoint: { hungryLevel: 1, satisfactionLevel: 2, healthLevel: 0, intelligenceLevel: 0 }, reaction: {
            reactionTxt: "J'ai tout mangé !", reactionImg: "https://2.bp.blogspot.com/-ZArsHhK7b9c/W3abF17xoFI/AAAAAAABN-c/CTwvqbJdeWY3AFI0EMKFYXbCK5gAtUuDACLcBGAs/s800/cat3_2_heart.png"
        }
    },
]
// List des objets concernant la partie healthcare(type:Feed)
let healthcares: Feed[] = [
    { effect: cleanToilet, item: "Nettoyer sa caisse", itemImg: toilet, minusPoint: { hungryLevel: 0, satisfactionLevel: 0, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 1, satisfactionLevel: 1, healthLevel: 2, intelligenceLevel: 0 }, reaction: { reactionTxt: "C'est tout propre !", reactionImg: "https://2.bp.blogspot.com/-ZArsHhK7b9c/W3abF17xoFI/AAAAAAABN-c/CTwvqbJdeWY3AFI0EMKFYXbCK5gAtUuDACLcBGAs/s800/cat3_2_heart.png" } },
    { item: "brosser", itemImg: brush, minusPoint: { hungryLevel: 0, satisfactionLevel: 0, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 1, satisfactionLevel: 1, healthLevel: 2, intelligenceLevel: 0 }, reaction: { reactionTxt: "Finies les boules de poils !", reactionImg: "https://1.bp.blogspot.com/-LQcOGNnbR_w/V9vCloWP19I/AAAAAAAA9-4/GCC32iYQvi8yyhmgwpIMS7uq7ixc0BiIQCLcB/s800/pet_niteru_cat.png" } },
    {
        effect: gotoVT, item: "Emmener chez le véto", itemImg: takeVt, minusPoint: { hungryLevel: 0, satisfactionLevel: 0, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 1, satisfactionLevel: 1, healthLevel: 2, intelligenceLevel: 0 }, reaction: { reactionTxt: "En pleine forme !", reactionImg: "http://4.bp.blogspot.com/-ZP5h3iFkY-w/VmFjpRYPiiI/AAAAAAAA1Xs/p7d7UyQSagE/s180-c/pet_kenkou_shindan_cat.png" }
    }
]
//effect(function) de chaque gift dans list 'healthcares'
/* variable noChange(type:boolean), s'il est 'true', on ne change pas les elements dans 'conditions' et 'totalPoint' de defaultcat suivant dans function showReaction; */
let noChange = false;
/** Fonction qui va ajouter des cssElement selon 'imageUnchiExist', si 'false', change noChange 'true';
* de l'objet Cat
*/
function cleanToilet(): void {
    if (imageUnchiExist) {
        imageUnchi?.classList.add('move')
        imageUnchiExist = false;
        setTimeout(() => {
            imageUnchi?.classList.add("d-none")
        }, 3000);
    } else {
        noChange = true;
    }
}
/** Fonction qui va ajouter des cssElement selon'virusExist'(boolean), si 'false', change noChange 'true';
* de l'objet Cat
*/
function gotoVT() {
    if (virusExist) {
        imageVirus?.classList.add('move')
        virusExist = false;
        setTimeout(() => {
            imageVirus?.classList.add("d-none")
        }, 3000);
    } else {
        noChange = true;
    }
}


// List des objets concernant la partie play game(type:Feed)
let plays: Feed[] = [
    { effect: rockpaperScissors, item: "Chifumi", itemImg: janken, minusPoint: { hungryLevel: 1, satisfactionLevel: 0, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 1, satisfactionLevel: 1, healthLevel: 2, intelligenceLevel: 2 }, reaction: { reactionTxt: "Je vais encore gagner !", reactionImg: "https://2.bp.blogspot.com/-ZArsHhK7b9c/W3abF17xoFI/AAAAAAABN-c/CTwvqbJdeWY3AFI0EMKFYXbCK5gAtUuDACLcBGAs/s800/cat3_2_heart.png" } },

    { effect: slotMachine, item: "Machine à sous ", itemImg: slot, minusPoint: { hungryLevel: 2, satisfactionLevel: 0, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 0, satisfactionLevel: 2, healthLevel: 2, intelligenceLevel: 1 }, reaction: { reactionTxt: "Gagne des sous  pour mes cadeaux !", reactionImg: "https://2.bp.blogspot.com/-ZArsHhK7b9c/W3abF17xoFI/AAAAAAABN-c/CTwvqbJdeWY3AFI0EMKFYXbCK5gAtUuDACLcBGAs/s800/cat3_2_heart.png" } },

    { effect: ballgame, item: "Baballe", itemImg: ball, minusPoint: { hungryLevel: 3, satisfactionLevel: 0, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 1, satisfactionLevel: 2, healthLevel: 3, intelligenceLevel: 1 }, reaction: { reactionTxt: "Un peu de cardio !", reactionImg: "https://2.bp.blogspot.com/-ZArsHhK7b9c/W3abF17xoFI/AAAAAAABN-c/CTwvqbJdeWY3AFI0EMKFYXbCK5gAtUuDACLcBGAs/s800/cat3_2_heart.png" } }
]
//effect(function) de chaque gift dans list 'plays'
// pour accéder à rock-paper-scissors section
const rpsSection = document.querySelector("#rpsSection");
function rockpaperScissors() {
    gameisStart = true;
    setTimeout(() => {
        changeSection(modalPlay, playBtn)
        changeSection(playSection, rpsSection)
    }, 1000)
}
// pour accéder à slotMachine section
const slotSection = document.querySelector("#slotSection");
function slotMachine() {
    gameisStart = true;
    setTimeout(() => {
        changeSection(modalPlay, playBtn)
        changeSection(playSection, slotSection)
    }, 1000)
}
// pour accéder à ballSection section
const ballSection = document.querySelector("#ballSection");
function ballgame() {
    gameisStart = true;
    setTimeout(() => {
        changeSection(modalPlay, playBtn)
        changeSection(playSection, ballSection)
    }, 1000)
}



// List des objets concernant la partie gift(type:Feed)
let gifts: Feed[] = [
    { effect: () => giveGift(20), item: "Nouvelle balle : 20points", itemImg: "https://3.bp.blogspot.com/-5Amsx8iWzrY/VxTO8HOSLtI/AAAAAAAA5-s/CNMdpDaK3Sc0mZ2fo5XHOKkPezAueqCBACLcB/s180-c/baseball_ball.png", minusPoint: { hungryLevel: 0, satisfactionLevel: 0, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 0, satisfactionLevel: 2, healthLevel: 1, intelligenceLevel: 1 }, reaction: { reactionTxt: "Amusant !", reactionImg: "https://4.bp.blogspot.com/-1865joK51EI/Wb8gnbhjDpI/AAAAAAABGy4/ByWIh6Zt-hM-CCEzYWBxcuTJOOWz26kYwCLcBGAs/s800/pet_omocha_neko.png" } },

    { effect: () => { giveGift(40) }, item: "Souris  : 40points", itemImg: giftmouse, minusPoint: { hungryLevel: 0, satisfactionLevel: 0, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 1, satisfactionLevel: 2, healthLevel: 2, intelligenceLevel: 2 }, reaction: { reactionTxt: "Je vais l'attraper !", reactionImg: "https://2.bp.blogspot.com/-qr9IA1O5840/WR_KiuxcTkI/AAAAAAABEXQ/8ci-guAuBZgyyXdvgS76XDKFng4G7m3BQCLcB/s400/cat_nezumi_oikakeru.png" } },

    { effect: () => { giveGift(80) }, item: "Coussin  : 80points", itemImg: giftcushion, minusPoint: { hungryLevel: 0, satisfactionLevel: 0, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 1, satisfactionLevel: 3, healthLevel: 2, intelligenceLevel: 0 }, reaction: { reactionTxt: "Parfait pour la sieste !", reactionImg: "https://3.bp.blogspot.com/-5Bbn6ugAN-Y/VxC3Q0PhdeI/AAAAAAAA51g/qBXo_EcdfCsl0-xeI156InckHV5hO2blgCLcB/s400/kagu_cushion.png" } },

    { effect: () => { giveGift(120) }, item: "Arbre à chat : 120points", itemImg: giftcattree, minusPoint: { hungryLevel: 0, satisfactionLevel: 0, healthLevel: 0, intelligenceLevel: 0 }, plusPoint: { hungryLevel: 0, satisfactionLevel: 4, healthLevel: 2, intelligenceLevel: 0 }, reaction: { reactionTxt: "En avant pour l'escalade !", reactionImg: "https://2.bp.blogspot.com/-ScrPvERO_Fk/Wb8gkP8opgI/AAAAAAABGyk/LSoFp1enzD4M7mRDg2O22-mK_r0SApyUgCLcBGAs/s400/pet_cat_tower.png" } }
]
//effect(function) de chaque gift dans list 'gifts'
/* pour chaque gift(ball,toy,cushion,Cattree),diminuer le pointGift selon son prix,  si il n'y a pas assez de points(pointGift), return noChange "true" et ne change rien.   */

function giveGift(num: number): void {
    if (defaultcat.pointGift >= num) {
        defaultcat.pointGift -= num;
    } else {
        noChange = true;
    }
}



/**
*  Function qui va générer un HTML Element (ul,img) et mettre un contenu/css element sur HTML afin de faire une list
* et l'afficher sur l'écran
* @section :une div Type(Element) qui va append une list
* @list :un List([]) object Type(feed) 
*/
function settingChoices(section: Element | null, list: Feed[]): void {
    const ul = document.createElement('ul');
    ul.classList.add("list-unstyled", "row");
    for (const ele of list) {
        const li = document.createElement('li');
        li.classList.add("col", "text-center");
        const p = document.createElement('p');
        p.textContent = ele.item;
        p.classList.add("d-none", "d-md-block");
        li.appendChild(p);
        const img = document.createElement("img");
        img.classList.add("imglist", "d-inlineblock")
        img.src = ele.itemImg;
        li.appendChild(img);
        ul.appendChild(li);
        section?.appendChild(ul);
        li.addEventListener('click', (event) => {
            event.preventDefault()
            if (ele.effect) {
                ele.effect();
            }

            defaultcat.commitCounter += 1
            showReaction(ele)
            setTimeout(() => {
                settingSection(defaultcat)
                imageOfcat?.classList.add("movecat")
            }, 3000);
        });
    }
}
/**
*  Function qui va mettre un contenu dans un HTMLelement et l'afficher sur l'écran selon l'objet donné en paramètre
* @action : un objet(Type:feed)
*/

function showReaction(action: Feed) {
    changeSection(modalHealth, healthBtn)
    changeSection(modalFeed, feedBtn);
    changeSection(modalGift, giftBtn);
    if (noChange == true) {
        if (imageOfcat) {
            imageOfcat.src = "https://1.bp.blogspot.com/-rpiwTyIxwsM/W3abFVvEpyI/AAAAAAABN-U/GEa5CnODincKOOSuzpVnWFBld4T2U5BXwCLcBGAs/s800/cat2_4_think.png";
        }
        if (catComment) {
            catComment.textContent = "Ce n'est pas le moment de faire ça."
        }
    }
    else {
        if (imageOfcat) {
            imageOfcat.src = action.reaction.reactionImg;
        }
        if (catComment) {
            catComment.textContent = `${action.reaction.reactionTxt}`
        }
        changeHungrylevel(action.minusPoint.hungryLevel, action.plusPoint.hungryLevel)
        changeSatisfactionlevel(action.minusPoint.satisfactionLevel, action.plusPoint.satisfactionLevel)
        changeHealthlevel(action.minusPoint.healthLevel, action.plusPoint.healthLevel)
        changeInglevel(action.minusPoint.intelligenceLevel, action.plusPoint.intelligenceLevel)
    }
    noChange = false;
}

settingChoices(listFeeddiv, feeds);
settingChoices(listHealthdiv, healthcares);
settingChoices(listPlaydiv, plays);
settingChoices(listGiftdiv, gifts);

/* Section rock-papaer-scissors */
let catBalloon = document.querySelector(".balloon1")
const patImg = document.querySelector<HTMLImageElement>(".pat");
const catsrcImg = document.querySelector<HTMLImageElement>(".catrps")
let jankenBtns = document.querySelectorAll<HTMLButtonElement>("#janken");
let commentCat = document.querySelector("#commentCat")
const startPrsbtn = document.querySelector("#startPrs");
const rspbtnsDiv = document.querySelector("#rspbtns");
let jankenPoint = 0;
let pointJankenSpan = document.querySelector("#pointJanken");
//startbutton
startPrsbtn?.addEventListener('click', () => {
    changeSection(startPrsbtn, rspbtnsDiv);
    if (patImg) {
        patImg.classList.add("movepat");
    }
    if (commentCat) {
        commentCat.textContent = "Chi fu MI !"
    }
})
//jankenbutton 
for (let i = 0; i < 3; i++) {
    jankenBtns[i].addEventListener('click', () => {
        jankenBtns[i].classList.add("selected");
        startRps(i);
    })
}
/**
*  Function qui va mettre cathand(random), juger et afficher un resultat,remettre setting selon un index du bouton appuyé
* @index : 0(index of Rock jankenBtn) or 1(index of Paper jankenBtn) or 2(index of Scissor jankenBtn)
*/
function startRps(index: number) {
    //mouvement de cat pat
    if (patImg) {
        patImg.classList.remove("movepat")
        patImg.src = "https://1.bp.blogspot.com/-m7usphTzjkE/YAOpdyGQJyI/AAAAAAABdPE/kxNo88K2gRAsoXG9pabCQBZg_N3ShE-aQCNcBGAsYHQ/s484/cat_hand_white.png"
    }
    // button setting
    for (let button of jankenBtns) {
        if (button?.classList.contains("selected")) {
            button.disabled = true
        } else {
            button.classList.add('disabled')
        }
    }
    //setting de cat hand (random)
    let catHands = ["https://2.bp.blogspot.com/-VhlO-Yfjy_E/Uab3z3RNJQI/AAAAAAAAUVg/fX8VnSVDlWs/s220/janken_gu.png", "https://4.bp.blogspot.com/-__yEIXe5SxU/Uab3zO7BB2I/AAAAAAAAUVI/MYg6TVeiv-Y/s270/janken_choki.png", "https://3.bp.blogspot.com/-qZtyoue9xKs/Uab30IG0Q5I/AAAAAAAAUVk/qnH8a2OgrvI/s290/janken_pa.png"];
    let patIndex = Math.floor(Math.random() * 3);
    if (catsrcImg)
        catsrcImg.src = catHands[patIndex];
    //commentaire et image de chat 
    if (catBalloon)
        catBalloon.classList.remove("d-none");
    let chatFaceImg = document.querySelector("#chatFace") as HTMLImageElement
    judgeRps()
    // remettre setting 
    setTimeout(() => {
        catBalloon?.classList.remove("action");
        if (patImg)
            patImg.src = "https://1.bp.blogspot.com/-nryYT65XAq0/YAOpd8Eg_8I/AAAAAAABdPI/xgYYoJ91ERI-U8dSgffSZJZirrgBhbjpACNcBGAsYHQ/s200/cat_hand_white_back.png";
        for (let button of jankenBtns) {
            if (button.classList.contains('selected')) {
                button.classList.remove("selected");
            } else {
                button.classList.remove('disabled');
            }
        }
        if (commentCat) {
            commentCat.textContent = "Essaie encore !"
        } if (catBalloon) {
            catBalloon.classList.add("d-none");
        } if (chatFaceImg) {
            chatFaceImg.src = "https://1.bp.blogspot.com/-TyouviIi_Bw/VOsJoqlxOcI/AAAAAAAArqw/a5cIK-wQyew/s300/cat4_laugh.png";
        }
        if (patImg) {
            patImg.classList.add("movepat");
        }
    }, 1000)

    //juger le résultat en comparant parametre(index)et patIndex 
    function judgeRps() {
        if ((patIndex == 0 && index == 1) || (patIndex == 1 && index == 2) || (patIndex == 2 && index == 0)) {
            if (commentCat) {
                commentCat.textContent = "Tu as perdu !"
            }
            if (chatFaceImg) {
                chatFaceImg.src = "https://2.bp.blogspot.com/-Wq39D5Sl7p4/W3abG77UVSI/AAAAAAABN-k/PH27NjOmYvMdxWPkIHMrU-j0-MtXbOCfgCLcBGAs/s300/cat3_4_tehe.png"
            }
        } if ((patIndex == 1 && index == 0) || (patIndex == 2 && index == 1) || (patIndex == 0 && index == 2)) {
            if (commentCat)
                commentCat.textContent = "Tu as gagné !"
            jankenPoint += 5
            defaultcat.pointGift += 5
            if (pointJankenSpan)
                pointJankenSpan.textContent = `${jankenPoint}`
            if (chatFaceImg) {
                chatFaceImg.src = "https://2.bp.blogspot.com/-0biYnJ_s6-0/VOsJobVp4pI/AAAAAAAArqo/PFc0-liMkqM/s300/cat3_cry.png"
            }
        } if (patIndex == index) {
            if (commentCat) {
                commentCat.textContent = "Égalité"
            }
            if (chatFaceImg) {
                chatFaceImg.src = "https://1.bp.blogspot.com/-OTqeO3wl5xU/VOsJndzmvaI/AAAAAAAArqY/eMQywXd1btc/s300/cat1_smile.png"
            }
        }
    }
}
// bouton pour quitter et remettre à zéro les conditions du jeu     
const quitButton2 = document.querySelector("#returnbtn6");
const modalJanken = document.querySelector("#modalRps");
const jankenDiv = document.querySelector("#jankenDiv");
quitButton2?.addEventListener('click', () => {
    gameisStart = false;
    if (modalJanken) {
        changeSection(jankenDiv, modalJanken)
    }
    if (patImg) {
        patImg.classList.remove("movepat");
    }
    setTimeout(() => {
        changeSection(rpsSection, playSection);
        changeSection(modalJanken, jankenDiv);
        changeSection(rspbtnsDiv, startPrsbtn);
        settingSection(defaultcat)
        imageOfcat?.classList.add("movecat")
        if (commentCat) {
            commentCat.textContent = "Appuie sur start"
        }
        jankenPoint = 0;
        if (pointJankenSpan)
            pointJankenSpan.textContent = "0";
    }, 2000)

})


/* Section slot */

class Panel {
    img: any; stop: any;
    //utiliser pour clearTimeout
    timeOutId: any;
    constructor() {
        //générer un HTML 
        const section = document.createElement("section");
        section.classList.add("panel");
        this.img = document.createElement("img");
        this.img.src = "https://1.bp.blogspot.com/-mc7mCgrV13s/V0QnZrx5NnI/AAAAAAAA64U/iHjLa4BL3zElJdC5GkfDeCuwS7bKWDY8QCLcB/s800/cat_fish_run.png"
        this.stop = document.createElement("div");
        this.stop.textContent = "STOP"
        this.stop.classList = "btn btn-primary stop mt-2 inactive"
        this.stop.addEventListener('click', () => {
            if (this.stop.classList.contains('inactive')) {
                return
            }
            this.stop.classList.add('inactive')
            clearTimeout(this.timeOutId);
            panelLeft--;
            if (panelLeft == 0) {
                checkPanel();
                spin?.classList.remove('inactive');
                panelLeft = 3;
            }
        })
        section.appendChild(this.img);
        section.appendChild(this.stop)

        const main = document.querySelector("main")
        main?.appendChild(section)
    }
    //pour mettre image d'une façon random
    getRandomimg() {
        const images = ["https://1.bp.blogspot.com/-mc7mCgrV13s/V0QnZrx5NnI/AAAAAAAA64U/iHjLa4BL3zElJdC5GkfDeCuwS7bKWDY8QCLcB/s800/cat_fish_run.png", "https://2.bp.blogspot.com/-vuUfiaLQMS8/U-8FsRv9e-I/AAAAAAAAkwc/QHbekOs4Xls/s400/pet_food_cat.png", "https://3.bp.blogspot.com/-IzBBa1iaxGc/XLQNJ_ysffI/AAAAAAABSbw/hgX31eDYY6QX5btrmZTNuMDm9JQL8B1ygCLcBGAs/s400/uchidenokoduchi_eto13_neko.png"]
        return images[Math.floor(Math.random() * images.length)]
    }
    // pour mettre image consectivement pour slot
    spin() {
        this.img.src = this.getRandomimg()
        this.timeOutId = setTimeout(() => {
            this.spin()
        }, 50)
    }
    activate() {
        this.stop.classList.remove('inactive')
    }
    isEqual(p1: any, p2: any) {
        return this.img.src == p1.img.src && this.img.src == p2.img.src
    }
}
// créér trois instances dans list panels
const panels = [new Panel(), new Panel(), new Panel()]
//pour compter number de panel
let panelLeft: number = 3;
//pour afficher les points gagnés et comment sur HTML
const pointCount = document.querySelector("#slotCounter")
let point = 0;
if (pointCount)
    pointCount.textContent = `${point}`
const slotComment = document.querySelector("#slotComment")
if (slotComment)
    slotComment.textContent = "Appuie sur Start"
const spin = document.querySelector("#spin")
spin?.addEventListener('click', () => {
    if (slotComment)
        slotComment.textContent = "Appuie sur chaque boutton STOP"
    //pour éviter de clicker le même bouton plusieurs fois
    if (spin.classList.contains('inactive')) {
        return
    }
    spin.classList.add('inactive')
    for (const panel of panels) {
        panel.activate();
        panel.spin();
    }
});
//juger le résultat et afficher sur HTML 
function checkPanel() {
    if (panels[0].isEqual(panels[1], panels[2])) {
        point += 20
        defaultcat.pointGift += 20;
        if (pointCount)
            pointCount.textContent = `${point}`;
        if (slotComment)
            slotComment.textContent = `Jackpot!! Tu remportes ${point} points!!`
    } else {
        if (slotComment) {
            slotComment.textContent = "Essaie à nouveau!!"
        }
    }
}
// un button pour quitter le jeux et remettre à zero les conditions du jeux
const quitButton = document.querySelector("#returnbtn5");
const modalSlot = document.querySelector("#modalSlot");
const slotDiv = document.querySelector("#slotdiv")
quitButton?.addEventListener('click', () => {
    if (modalSlot)
        modalSlot.classList.remove("d-none");
    if (slotDiv)
        slotDiv.classList.add("d-none");
    // pour arreter 
    gameisStart = false;
    setTimeout(() => {
        changeSection(slotSection, playSection);
        resetSlot()
        slotDiv?.classList.remove("d-none");
        modalSlot?.classList.add("d-none");
        modalSlot?.classList.add("d-none");
        settingSection(defaultcat)
        imageOfcat?.classList.add("movecat")
    }, 2000)
})
//function pour remettre à zero les conditions du jeux
function resetSlot() {
    if (slotComment)
        slotComment.textContent = "Appuie sur Start";
    point = 0;
    if (pointCount)
        pointCount.textContent = `${point}`;
}

// Section ballgame
const patDiv = document.querySelector("#ballpat");
const openingBallDiv = document.querySelector<HTMLDivElement>("#openingBallDiv");
const canvasDiv = document.querySelector<HTMLDivElement>("#canvasDiv");
const startBallbtn = document.querySelector("#startBall")
const readyH4 = document.querySelector("#readygo")
const imagePat = document.querySelector("#ballpat")
const scoreSpan = document.querySelector("#pointBall")
const quitBallbtn = document.querySelector("#returnbtn7")
const canvas = document.querySelector('canvas');
let countdown = 3;
//start button
startBallbtn?.addEventListener('click', () => {
    changeSection(openingBallDiv, canvasDiv);
    startBallbtn.classList.add("d-none");
    if (scoreSpan)
        scoreSpan.textContent = "0";
    //afficher countdown
    if (readyH4)
        readyH4.classList.remove("d-none");
    let intervalId = setInterval(() => {
        if (countdown == 0) {
            if (readyH4)
                readyH4.classList.add('d-none')
            clearInterval(intervalId)
        }
        if (readyH4)
            readyH4.textContent = `${countdown}`
        countdown--
    }, 1000)
    // commencer un jeux
    setTimeout(() => {
        new Game(canvas)
        patDiv?.classList.remove("d-none")
        imagePat?.classList.add("movepat")
        quitBallbtn?.classList.remove('d-none')
    }, 3500)
})

class Ball {
    canvas: any; ctx: any; x: number; y: number; r: number; vx: number; vy: number; isMissed: boolean;
    constructor(canvas: any) {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');
        this.x = 30;
        this.y = 30;
        this.r = 10;
        //
        this.vx = 3;
        this.vy = 2;
        //pour finir le jeux
        this.isMissed = false;
    }
    getMissedStatus() {
        return this.isMissed;
    }
    getX() {
        return this.x;
    }
    getY() {
        return this.y;
    }
    getR() {
        return this.r;
    }
    //method pour changer le direction pour se déplacer
    bounce() {
        this.vy *= -1
    }
    //pour mettre le ball qui est au dessus jusqu'à top de paddle; 
    reposition(paddleTop: number) {
        this.y = paddleTop - this.r
    }
    //method qui change le position et le direction de ball, quand il touche le border, il va changer le direction en opposite.
    update() {
        this.x += this.vx;
        this.y += this.vy;
        if (this.y - this.r > this.canvas.height) {
            this.isMissed = true;
        } if (this.x - this.r < 0 || this.x + this.r > this.canvas.width) {
            this.vx *= -1;
        } if (this.y - this.r < 0) {
            this.vy *= -1;
        }
    }
    //method qui dessiner un ball sur canvas
    draw() {
        this.ctx.beginPath();
        this.ctx.fillStyle = '#fdfdfd';
        this.ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI);
        this.ctx.fill();
    }
}

class Paddle {
    canvas: any; game: any; ctx: any; w: number; h: number; x: number; y: number; mouseX: number
    constructor(canvas: any, game: any) {
        this.canvas = canvas;
        this.game = game;
        this.ctx = this.canvas.getContext('2d');
        //w:width, h:height,x,y:position sur canvas de barre(Paddle)
        this.w = 60;
        this.h = 16;
        this.x = this.canvas.width / 2 - (this.w / 2);
        this.y = this.canvas.height - 32;
        this.mouseX = this.x;
        this.addHandler();
    }
    //method syncronize le mousemove et le deplacement de barre(paddle)
    addHandler() {
        document.addEventListener('mousemove', e => {
            this.mouseX = e.clientX;
        });
    }
    //
    update(ball: any) {
        const ballBottom = ball.getY() + ball.getR();
        const paddleTop = this.y;
        const ballTop = ball.getY() - ball.getR();
        const paddleBottom = this.y + this.h;
        const ballCenter = ball.getX();
        const paddleLeft = this.x;
        const paddleRight = this.x + this.w
        // si ball touche paddle, changer le direction de ball(bounce()), déplacer ball jusqu'a top de barre(paddle), et le score 
        if (ballBottom > paddleTop && ballTop < paddleBottom && paddleLeft < ballCenter && ballCenter < paddleRight) {
            ball.bounce()
            ball.reposition(paddleTop)
            this.game.addScore()
        }
        //pour ajuster le position de barre(Paddle)
        const rect = this.canvas.getBoundingClientRect();
        this.x = this.mouseX - rect.left - (this.w / 2);
        if (this.x < 0) {
            this.x = 0;
        } if (this.x + this.w > this.canvas.width) {
            this.x = this.canvas.width - this.w;
        }
    }
    //pour dessiner barre(Paddle)
    draw() {
        this.ctx.fillStyle = '#fdfdfd';
        this.ctx.fillRect(this.x, this.y, this.w, this.h);
    }
}

class Game {
    canvas: any; ctx: any; ball: any; paddle: any; isGameOver: boolean; score: number;
    constructor(canvas: any) {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');
        this.ball = new Ball(this.canvas);
        this.paddle = new Paddle(this.canvas, this);
        this.loop();
        this.isGameOver = false;
        this.score = 0;
    }
    addScore() {
        this.score += 5;
        defaultcat.pointGift += 5;
        if (scoreSpan) {
            scoreSpan.textContent = `${this.score}`
        }
    }
    // repeter,looper update() et draw() 
    loop() {
        if (this.isGameOver) {
            return;
        }
        this.update();
        this.draw();
        //Mettre un animation 
        requestAnimationFrame(() => {
            this.loop();
        });
    }

    update() {
        this.ball.update();
        this.paddle.update(this.ball);
        if (this.ball.getMissedStatus()) {
            this.isGameOver = true;
        }
    }
    //pour afficher les elements sur l'écran sauf isGameOver is true
    draw() {
        if (this.isGameOver) {
            this.drawGameOver();
            return
        }
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ball.draw();
        this.paddle.draw();
        this.drawScore();
    }
    //pour afficher un GameOver et remettre à zero 
    drawGameOver() {
        this.ctx.font = '28px "Arial Black"'
        this.ctx.fillStyle = 'tomato'
        this.ctx.fillText('GAME OVER', 50, 150)
        imagePat?.classList.remove("movepat")
        setTimeout(() => {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        }, 2000)
    }
    drawScore() {
        this.ctx.font = '20px Arial';
        this.ctx.fillStyle = "#fdfdfd";
        this.ctx.fillText(this.score, 10, 25);
    }
}

//quit button
const quitButton3 = document.querySelector("#returnbtn7");
const modalBall = document.querySelector("#modalBall");
const divBall = document.querySelector("#divBall")
/* function qui remettre remettre à zéro  */
function resetBall() {
    countdown = 3;
    imagePat?.classList.add("d-none");
    changeSection(modalBall, divBall)
    startBallbtn?.classList.remove("d-none")
    changeSection(canvasDiv, openingBallDiv)
    if (readyH4)
        readyH4.textContent = `Ready?`
}
//pour quitter le jeux et remettre à zero les elements 
quitButton3?.addEventListener('click', () => {
    gameisStart = false;
    if (modalBall) {
        changeSection(divBall, modalBall)
    }
    setTimeout(() => {
        changeSection(ballSection, playSection);
        settingSection(defaultcat)
        imageOfcat?.classList.add("movecat")
        resetBall();
    }, 2000)
})
